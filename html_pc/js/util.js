(function() {

	this.ua = function(){
		var conf = {
			isSphone : false ,
			isTablet : false ,
			isPc : true ,
			isCss3 :true ,
			isScale:true,
			isIe : false ,
			isOpera : false ,
			isFF : false ,
			isChrome : false ,
			isSafari : false ,
			brVer : null ,
			isMac:false
		}

		var ua = navigator.userAgent.toLowerCase();

		if(ua.match(/mac|ppc/)){
			conf.isMac = true;
		}

		if(ua.indexOf('ipad') != -1) {
			conf.isTablet = conf.isCss3 = true;
			conf.isPc = false;
		} else if(ua.indexOf('iphone') != -1 || ua.indexOf('ipod') != -1) {
			conf.isSphone = conf.isCss3 = true;
			conf.isPc = false;
		} else if(ua.indexOf('android') != -1){
			conf.isCss3 = true;
			conf.isPc = false;

			if(ua.indexOf('mobile') != -1) {
				conf.isSphone = true;
			} else {
				conf.isTablet = true;
			}
		}


		if (ua.indexOf('opera') != -1) {
			conf.isOpera = true;
		} else if (ua.indexOf('msie') != -1 || ua.indexOf('trident') != -1) {
			var ver = navigator.appVersion.toLowerCase();
			conf.isIe = true;
			var tmpArr = /(msie|rv:?)\s?([0-9]{1,})([\.0-9]{1,})/.exec(ua);

			if(tmpArr) {
				conf.brVer = parseInt(tmpArr[2], 10);

				if(conf.brVer <= 9) {
					conf.isCss3 = false;
					conf.isScale = false;
				}
			} else {
				conf.isCss3 = false;
				conf.isScale = false;
			}


		} else if (ua.indexOf('chrome') != -1) {
			conf.isChrome = true;
		} else if (ua.indexOf('safari') != -1) {
			conf.isSafari = true;
		} else if (ua.indexOf('firefox') != -1) {
			conf.isFF = true;
		}

		return conf;
	}

	this._int = function _int(num){
		if(typeof num == 'string') {
			return parseInt(num.replace('px' , '') , 10);
		} else if(num){
			return parseInt(num , 10);
		} else {
			return 0;
		}
	}

	this.taranend = "webkitTransitionEnd oTransitionEnd otransitionend transitionend";

	this.css_vendor_txt = '';

	var tmp = this.ua();

	if(tmp.isIe){
		this.css_vendor_txt = '-ms-';
	} else if(tmp.isFF) {
		this.css_vendor_txt = '-moz-';
	} else if(tmp.isOpera) {
		this.css_vendor_txt = '-o-';
	} else{
		this.css_vendor_txt = '-webkit-';
	}

	// コーナー4つのポジションを保持したobjectを返します
	this._get_corner_positions = function(left, top, width, height){
	    var obj = [];
	    obj.push({left: left, top: top}); //左上
	    obj.push({left: left + width, top: top}); //右上
	    obj.push({left: left, top: top + height}); //左下
	    obj.push({left: left + width, top: top + height}); //右下
	    return obj;
	};

	/**
	 * posObj1がposObj2が重なってたらtrueを返します
	 */
	this._isLapObjPos = function(posObj1 , posObj2){
		// if(X0 < X3 && X2 < X1 && Y0 < Y3 && Y2 < Y1)
		if(posObj1.l < posObj2.l + posObj2.w
			&& posObj2.l < posObj1.l + posObj1.w
			&& posObj1.t < posObj2.t + posObj2.h
			&& posObj2.t < posObj1.t + posObj1.h
		) {
//			console.debug("重なり");
			return true;
		} else {
//			console.debug("none!");
			return false;
		}
	};

	this.rand_len = function(min , max){
    	return Math.floor(Math.random()*(max - min))+min;
    }

    this.rand = function(n) {
        return Math.floor(Math.random()*n)+1;
    }

}).call(this);