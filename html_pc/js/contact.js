$(function(){
// ボタンのホバー
    $('#contact_form button,#contact_form a').hover(
		function() {
			$(this).find('span').stop(true).animate(
				{'opacity': 1},350,'easeOutQuart'
			);
		}, function() {
			$(this).find('span').stop(true).animate(
				{'opacity': 0},350
			);
		}
	);
	
// checkbox選択時の処理
    $('#contact_form .input_checkbox').on('click',function(){
    	if($(this).children('input').is(":checked")){
    		$(this).addClass('selected');
        } else{
            $(this).removeClass('selected');
        }
    });

// radio選択時の処理
	function radioChange(element){
	    $(element).on('click',function(){
	        $(element).removeClass('selected');
	        if($(this).children('input').change()){
	            $(this).addClass('selected');
	        }
	    });
	}
	radioChange('#radioes1 label');
	radioChange('#radioes2 label');

//〒ボタン動作
	$('#btn_zip').on('click',function(){
		AjaxZip3.zip2addr('zip21','zip22','pref21','addr21','strt21');
	});

// select装飾
	$('#contact_form select').customSelect();

// placeholderをIEで対応
	$('[placeholder]').ahPlaceholder({
		placeholderColor : 'silver',
		placeholderAttr : 'placeholder',
		likeApple : false
	});
});