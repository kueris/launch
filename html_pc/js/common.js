$(function(){
//ブラウザ判定
	var ua = window.ua();

// viewportをデバイスごとに指定
	if(ua.isTablet){
		var meta = document.createElement('meta');
		meta.setAttribute('name', 'viewport');
		meta.setAttribute('content', 'width=1070px,minimum-scale=0,maximum-scale=10');
		document.getElementsByTagName('head')[0].appendChild(meta);
	} else if(ua.isSphone){
		var meta = document.createElement('meta');
		meta.setAttribute('name', 'viewport');
		meta.setAttribute('content', 'width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0');
		document.getElementsByTagName('head')[0].appendChild(meta);
	}

 //ロード時イベント
 	// CSS3アニメーション
	if(ua.isCss3){
		$('#header,#contents,#footer').addClass('anm_fade');
	}

	// デバイスによってクラスを追記
	$(window).on("load",function(){	
		if(ua.isPc){
			$("html").addClass("pc");
		} else if(ua.isSphone){
			$("html").addClass("sp");
		} else if(ua.isTablet){
			$("html").addClass("tablet");
		}
		if(ua.isSafari){
			$("html").addClass("safari");
		} else if(ua.isFF){
			$("html").addClass("ff");			
		} else if(ua.isChrome){
			$("html").addClass("chrome");			
		}
	});

// スクロール
	var $document = $(document);

	$('a[href^="#"]').click(function(event) {
		if(!$(this).hasClass('nolink')){
			var id = $(this).attr("href"),
			offset = 10,
			target = $(id).offset().top - offset;
			m_stop_on();
			$('html, body').animate({scrollTop:target}, 500 , m_stop_off);
			event.preventDefault();
			return false;
		}
	});

	function m_stop_on(){
		$document.on('DOMMouseScroll', preventDefault);
	    $document.on('mousewheel', preventDefault);
	}

	function m_stop_off(){
		$document.off('DOMMouseScroll', preventDefault);
	    $document.off('mousewheel', preventDefault);
	}

	function preventDefault(e){
		event.preventDefault();
	}

// 旧式ブラウザのユーザーへ
	$(function () {
		if (typeof window.addEventListener == "undefined" && typeof document.documentElement.style.maxHeight == "undefined") {
			$('body').prepend('<div class="ie6_error">現在、旧式ブラウザをご利用中です。このウェブサイトは、現在ご利用中のブラウザには対応しておりません。バージョンを確認し、アップグレードを行ってください。</div>');
		}
	});

// ページトップへ
	if(ua.isPc){
		$('#btn_pt').hover(
			function() {
				$(this).children('span').animate(
					{'top': '39px'},350,'easeOutQuart'
				);
			}, function() {
				$(this).children('span').stop(true).animate(
					{'top': '44px'},350,'easeOutQuart'
				);
			}
		);
	}
});
