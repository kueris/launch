<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="keywords" content="">
<meta name="description" content="">
<meta property="fb:admins" content="FacebookID">
<meta property="og:type" content="website">
<meta property="og:locale" content="ja_JP">
<meta property="og:image" content="ogpURL">
<meta property="og:site_name" content="">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:url" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Launch!｜モチベーションの高いクリエイターが集うマッチングサイト</title>
<script src="js/min/jquery-1.11.0.min.js"></script>
<!--[if lt IE 9]>
<script src="js/min/html5shiv-printshiv.min.js"></script>
<script src="js/min/ie9.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="faviconURL">
<link rel="apple-touch-icon" href="WebClipIconURL">
<link href="css/top.css" rel="stylesheet" media="all">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
</head>
<body class="page_top">
<!--st Facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--ed Facebook-->
<noscript>
<div class="noScript">サイトを快適に利用するためには、JavaScriptを有効にしてください。</div>
</noscript>
<div id="wrapper">
	<header id="header">
		<div class="head_top">
			<div class="site_name">
				<a href="index.html">Launch!</a>
				<h1>モチベーションの高いクリエイターが集うマッチングサイト</h1>
			</div>
			<div class="top_menu" id="g_nav">
				<ul>
					<li>
						<a href="index.html" class="top_menu_a">
							<span class="nav_name">ログイン</span>
						</a>
					</li>
					<li>
						<a href="pc/news/index.html" class="top_menu_a">
							<span class="nav_name">会員登録</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<nav>
			<ul class="g_nav" id="g_nav">
				<li><a href="#"><span class="cat_name">プロジェクトを立ち上げる</span></a></li>
				<li><a href="#"><span class="cat_name">プロジェクト一覧を見る</span></a></li>
				<li><a href="#"><span class="cat_name">Launch!について</span></a></li>
			</ul>
		</nav>
	</header><!-- / #header -->

	<aside class="top_main">
		<a href="#">
			<div class="top_main_img"><img src="images/sample/main.jpg" height="800" width="2400" alt=""></div>
			<div class="top_main_txt">
				<h2>Launch! って？</h2>
				<p>テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。<br>
				テキストが入ります。テキストが入ります。テキストが入ります。</p>
			</div>
		</a>
	</aside>

	<ul class="menu_list">
		<li><a href="#" class="menu_cat0 active">すべて</a></li>
		<li><a href="#" class="menu_cat1">WEBサービス</a></li>
		<li><a href="#" class="menu_cat2">WEBアプリ</a></li>
		<li><a href="#" class="menu_cat3">コミュニティ</a></li>
		<li><a href="#" class="menu_cat4">その他</a></li>
	</ul>

	<div id="contents">
		<div class="top_contents" id="top_contents">
			<article class="box type1">
				<a href="#" class="box_inner">
					<span class="new">NEW!</span>
					<aside class="box_eye"><img src="images/sample/thumb1.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">Androidアプリを一緒に作りませんか！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webアプリ</li>
						<li class="mot">まったり</li>
						<li class="area">都内</li>
					</ul>
				</a>
			</article>
			<article class="box type2">
				<a href="#" class="box_inner">
					<span class="new">NEW!</span>
					<aside class="box_eye"><img src="images/sample/thumb2.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">起業目指してWEBサービスを作りませんか？</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webサービス</li>
						<li class="mot">ガチ</li>
						<li class="area">千葉</li>
					</ul>
				</a>
			</article>
			<article class="box type3">
				<a href="#" class="box_inner">
					<span class="like">LIKE!</span>
					<aside class="box_eye"><img src="images/sample/thumb3.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">定期的にJSの勉強会をしよう！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">コミュニティ</li>
						<li class="mot">きっちり</li>
						<li class="area">埼玉</li>
					</ul>
				</a>
			</article>
			<article class="box type4">
				<a href="#" class="box_inner">
					<span class="like">LIKE!</span>
					<aside class="box_eye"><img src="images/sample/thumb4.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">イケてる名刺をつくろう！の会　in大阪</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">その他</li>
						<li class="mot">ガチ</li>
						<li class="area">大阪</li>
					</ul>
				</a>
			</article>
			<article class="box type1">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb1.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">Androidアプリを一緒に作りませんか！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webアプリ</li>
						<li class="mot">まったり</li>
						<li class="area">都内</li>
					</ul>
				</a>
			</article>
			<article class="box type2">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb2.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">起業目指してWEBサービスを作りませんか？</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webサービス</li>
						<li class="mot">ガチ</li>
						<li class="area">千葉</li>
					</ul>
				</a>
			</article>
			<article class="box type3">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb3.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">定期的にJSの勉強会をしよう！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">コミュニティ</li>
						<li class="mot">きっちり</li>
						<li class="area">埼玉</li>
					</ul>
				</a>
			</article>
			<article class="box type4">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb4.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">イケてる名刺をつくろう！の会　in大阪</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">その他</li>
						<li class="mot">ガチ</li>
						<li class="area">大阪</li>
					</ul>
				</a>
			</article>
			<article class="box type1">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb1.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">Androidアプリを一緒に作りませんか！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webアプリ</li>
						<li class="mot">まったり</li>
						<li class="area">都内</li>
					</ul>
				</a>
			</article>
			<article class="box type2">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb2.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">起業目指してWEBサービスを作りませんか？</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">Webサービス</li>
						<li class="mot">ガチ</li>
						<li class="area">千葉</li>
					</ul>
				</a>
			</article>
			<article class="box type3">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb3.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">定期的にJSの勉強会をしよう！</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">コミュニティ</li>
						<li class="mot">きっちり</li>
						<li class="area">埼玉</li>
					</ul>
				</a>
			</article>
			<article class="box type4">
				<a href="#" class="box_inner">
					<aside class="box_eye"><img src="images/sample/thumb4.jpg" height="245" width="243" alt=""></aside>
					<div class="box_body">
						<h2 class="box_head_txt">イケてる名刺をつくろう！の会　in大阪</h2>
					</div>
					<ul class="box_foot">
						<li class="cat">その他</li>
						<li class="mot">ガチ</li>
						<li class="area">大阪</li>
					</ul>
				</a>
			</article>
		</div>
		<div class="pagination">
			<div class="wp-pagenavi">
				<a href="#" class="previouspostslink"><span>&lt;&lt;</span></a>
				<span class="current"><span>1</span></span>
				<a href="#"><span>2</span></a>
				<a href="#"><span>3</span></a>
				<a href="#"><span>4</span></a>
				<a href="#"><span>5</span></a>
				<a href="#"><span>6</span></a>
				<a href="#"><span>7</span></a>
				・・・
				<a href="#"><span>15</span></a>
				<a href="#" class="nextpostslink"><span>&gt;&gt;</span></a>
			</div>
		</div><!-- / .pagination -->
	</div>

	<footer id="footer">
		<a href="#wrapper" id="btn_pt"><span>TOPへ</span></a>
		<div class="foot_bottom">
			<ul class="foot_sns">
				<li class="tw_btn"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja" data-size="">ツイート</a></li>
				<li class="fb_btn"><div class="fb-like" data-send="false" data-href="http://liginc.co.jp" data-layout="button_count" data-width="450" data-show-faces="false"></div></li>
			</ul>
			<small>Launch! Allrights reserved.</small>
		</div>
	</footer><!-- / #footer -->
</div><!-- / #wrapper -->
<!--st js-->
<script src="js/min/util.min.js"></script>
<script src="js/min/easing.1.3.min.js"></script>
<script src="js/min/top.min.js"></script>
<script src="js/min/common.min.js"></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!--ed js-->
</body>
</html>